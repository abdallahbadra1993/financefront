import "./App.css";
import { withRouter, Switch, Route, Redirect } from "react-router-dom";
import GetAllAdmins from './components/GetAllAdmin/GetAllAdmins';
import CreateAdmin from './components/CreateAdmin/CreateAdmin';
import Popup from "./components/Popup/Popup";

function App() {
  
  return (
    <div className="App">
      <Switch>
        <Route path="/addadmin" component={CreateAdmin} />
        <Route path="/GetAllAdmins" component={GetAllAdmins} />
        <Route path="/Popup" component={Popup} />
      </Switch>
    </div>
  );
}

export default withRouter(App);
