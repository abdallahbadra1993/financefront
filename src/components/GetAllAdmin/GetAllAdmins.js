import React, { useState, useEffect } from "react";
import axios from "axios";
import AdminRecord from "../AdminRecord/AdminRecord";
export default function GetAllAdmins() {

const [Admins, setAdmins] = useState([]);
  
useEffect(() => {
  const request = async () => {
    await axios
      .get("http://localhost:8000/api/getalladmins")
      .then((response) => {
        setAdmins(response.data);
      });
  };
  request();
}, []);
 
 const renderHeader = () => {
   let headerElement = ["admin_id", "username", "password", "profile_picture"];

   return headerElement.map((key, index) => {
     return <th key={index}>{key.toUpperCase()}</th>;
   });
 };

 const renderBody = () => {
  
    return Admins.map((admin) => (
      <AdminRecord
        key={admin.admin_id}
        admin_id={admin.admin_id}
        username={admin.username}
        password={admin.password}
        profile_picture={admin.profile_picture}
        removeAdmin={removeAdmin}
      />
    ));
 }
 const removeAdmin = (admin_id) => {
   axios
     .delete(`http://localhost:8000/api/deleteadmin/${admin_id}`)
     .then((res) => {
       console.log(res);
       console.log(res.data);
     });
   // console.log(Admins);
   alert("are sure you want to remove");
   setAdmins(
     Admins.filter(function (obj) {
       return obj.admin_id !== admin_id;
     })
   );
   // console.log(Admins);
 };

 return (
   <div className="App">
     <p>{JSON.stringify(Admins)}</p>
    
     <table id="employee">
       <thead>
         <tr>{renderHeader()}</tr>
       </thead>
       <tbody>{renderBody()}</tbody>
     </table>
   </div>
 );
}


