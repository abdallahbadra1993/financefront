import React, {} from "react";
import {Link} from "react-router-dom";
export default function AdminRecord(props) {
  return (
    <tr key={props.admin_id}>
      <td>{props.admin_id}</td>
      <td>{props.username}</td>
      <td>{props.password}</td>
      <td>
        <img
          src={`http://localhost:8000/storage/uploads/${props.profile_picture}`}
          alt="Italian Trulli"
          width="100px"
          height="100px"
        ></img>
      </td>
      <td className="opration">
        <Link to="/pop">
          <button className="button">Edit heree</button>
        </Link>
        <button
          className="button"
          onClick={() => props.removeAdmin(props.admin_id)}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
