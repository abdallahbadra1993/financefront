import React, { useState } from "react";
import axios from "axios";
export default function CreateAdmins() {
const [username, setusername] = useState("");
const [password, setpassword] = useState("");
const [profile_picture, setprofile_picture] = useState();
const submitHandler = (e) => {
  e.preventDefault();
  console.log(profile_picture);
  const formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);
  formData.append("profile_picture", profile_picture);

  axios
    .post("http://localhost:8000/api/createadmin", formData, {
      headers: {
        "Accept": "application/json",
        "Content-Type": "multipart/form-data",
      },
    })
    .then((res) => {
      console.log(res);
      console.log(res.data);
    });
};
    return (
      <form onSubmit={submitHandler}>
        <div className="">
          <div className="">
            <p style={{ fontSize: "20px" }}> username</p>
          </div>
          <div className="">
            <input
              type="text "
              id="username"
              onChange={(e) => setusername(e.target.value)}
              name="username"
              value={username}
              style={{
                color: "black",
                border: "1px solid #ff1cd2",
                width: "400px",
                height: "40px",
              }}
            />
          </div>

          <div className="">
            <p style={{ fontSize: "20px" }}>password</p>
          </div>

          <div className="">
            <input
              type="text "
              id="password"
              onChange={(e) => setpassword(e.target.value)}
              name="password"
              value={password}
              style={{
                color: "black",
                border: "1px solid #ff1cd2",
                width: "400px",
                height: "40px",
              }}
            />
          </div>
          <div className="">
            <p style={{ fontSize: "20px" }}>profile picture</p>
          </div>

          <div className="">
            <input
              type="file"
              id="profile_picture"
              onChange={(e) => setprofile_picture(e.target.files[0])}
              name="profile_picture"
              //value={profile_picture}
            />
          </div>
          <div className="">
            <input type="submit" className="btn" />
          </div>
        </div>
      </form>
    );
}
